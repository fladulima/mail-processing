#Email Processing 

### Pre-Requisites

* Java 1.8+
* Hbase 2.2.0
* Elasticsearch 6.7.0
* Hadoop 3.1.2

### Build
```
./gradlew clean assemble
```

### Run

```
java -jar build/libs/mail-processing-*.jar \
 -Dmail.dataset.directory=<dataset directory> \
 -Dartifact.list=<provide a comma separated list of terms>
```

To get the features result
```
curl localhost:8080/features
```

To get primary feature 
```
curl localhost:8080/features/primary
```

To get secondary feature 
```
curl localhost:8080/features/secondary
```