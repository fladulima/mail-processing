package features

import com.bhvx.mail.test.persistence.PersistenceConstant
import com.bhvx.mail.test.util.MailHelper
import org.apache.hadoop.hbase.client.Put
import org.apache.hadoop.hbase.util.Bytes
import org.elasticsearch.index.query.QueryBuilders

import static com.bhvx.mail.test.persistence.PersistenceConstant.Feature.Qualifier
import static com.bhvx.mail.test.persistence.PersistenceConstant.Feature.VALUES

def artifactList = parameters['artifactList'] as String[]
println("Executing primary feature")
artifactList.each {
	def ticker = it as String

	def response = elasticsearchTemplate.client
			.prepareSearch("mail")
			.setQuery(QueryBuilders.matchQuery("body", ticker))
			.get()

	def hits = response?.getHits()?.getHits()
	hits.each {
		def mail = mailDao.findById(it.id)

		if (mail) {
			byte[] fileBytes = mail.originalFile
			def date = MailHelper.getDateFromBytes(fileBytes)
			final Put put = new Put(Bytes.toBytes(mail.getId()))
			put.addColumn(PersistenceConstant.Feature.INFO, Qualifier.NAME, Bytes.toBytes(PersistenceConstant.Feature.PRIMARY))
			put.addColumn(VALUES, Qualifier.DATE_FEATURE, Bytes.toBytes(date.toString()))
			put.addColumn(VALUES, Qualifier.EMAIL_LENGTH, Bytes.toBytes(fileBytes.length))
			put.addColumn(VALUES, Bytes.toBytes(String.format("%s@" + Qualifier.RELEVANCE_AS_STRING, ticker)), Bytes.toBytes(it.score))

			featureDao.save(put)
		}
	}
}
