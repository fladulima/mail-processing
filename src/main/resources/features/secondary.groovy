package features

import com.bhvx.mail.test.persistence.PersistenceConstant
import com.bhvx.mail.test.persistence.PersistenceConstant.Feature
import com.bhvx.mail.test.util.MailHelper
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.conf.Configured
import org.apache.hadoop.hbase.client.Put
import org.apache.hadoop.hbase.client.Result
import org.apache.hadoop.hbase.client.Scan
import org.apache.hadoop.hbase.filter.DependentColumnFilter
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil
import org.apache.hadoop.hbase.mapreduce.TableMapper
import org.apache.hadoop.hbase.mapreduce.TableReducer
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.io.DoubleWritable
import org.apache.hadoop.io.MapWritable
import org.apache.hadoop.io.Text
import org.apache.hadoop.mapreduce.Job
import org.apache.hadoop.util.Tool

import java.text.ParseException
import java.text.SimpleDateFormat

import static com.bhvx.mail.test.persistence.PersistenceConstant.Feature.VALUES

class SecondaryFeature extends Configured implements Tool {
	@Override
	int run(final String[] args) throws Exception {
		def config = new Configuration()

		def job = new Job(config, "secondary")
		job.setJarByClass(this.getClass())

		def scan = new Scan()
		scan.setCaching(500)
		scan.setCacheBlocks(false)

		scan.setFilter(
				new DependentColumnFilter(Feature.VALUES, Feature.Qualifier.EMAIL_LENGTH));

		TableMapReduceUtil.initTableMapperJob(Feature.TABLE,
				scan,
				FeatureMapper,
				Text.class,
				MapWritable.class,
				job)

		TableMapReduceUtil.initTableReducerJob(
				Feature.TABLE.toString(),
				FeatureReducer,
				job)

		return job.waitForCompletion(true) ? 0 : 1
	}

	static class FeatureMapper extends TableMapper<Text, MapWritable> {

		void map(ImmutableBytesWritable row, Result value, Context context) throws InterruptedException, IOException {
			byte[] date = value.getValue(Feature.VALUES, Feature.Qualifier.DATE_FEATURE)
			byte[] length = value.getValue(Feature.VALUES, Feature.Qualifier.EMAIL_LENGTH)

			final String dateValue = Bytes.toString(date)
			try {
				final SimpleDateFormat dateFeatureFormat = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH)
				final String mapKeyDate = new SimpleDateFormat("dd-MM-yyyy k").format(dateFeatureFormat.parse(dateValue))

				final MapWritable mapWritable = new MapWritable()
				mapWritable.put(new Text("length"), new DoubleWritable(Bytes.toInt(length)))

				value.getFamilyMap(Feature.VALUES).forEach({
					k, v ->
						if (Bytes.toString(k).contains(Feature.Qualifier.RELEVANCE_AS_STRING)) {
							mapWritable.put(new Text(Bytes.toString(k)), new Text(String.valueOf(Bytes.toFloat(v))))
						}
				})

				context.write(new Text(mapKeyDate), mapWritable)
			}
			catch (ParseException e) {
				println "Error while processing secondary feature"
			}
		}
	}

	static class FeatureReducer extends TableReducer<Text, MapWritable, ImmutableBytesWritable> {

		void reduce(Text key, Iterable<MapWritable> values, Context context) throws IOException, InterruptedException {
			final List<Double> emailLengths = new ArrayList<>()
			final Map<String, List<Double>> relevances = new HashMap<>()

			values.forEach { mapWritable ->
				mapWritable.forEach({
					k, v ->
						if (k.toString().equals("length")) {
							emailLengths.add(Double.parseDouble(v.toString()))
						} else {
							if (relevances.containsKey(k.toString())) {
								final List<Double> scores = relevances.get(k.toString())
								scores.add(Double.parseDouble(v.toString()))
								relevances.put(k.toString(), scores)
							} else {
								final List<Double> scores = new ArrayList<>()
								scores.add(Double.parseDouble(v.toString()))
								relevances.put(k.toString(), scores)
							}
						}
				})
			}

			double average = calculateAverage(emailLengths)
			final Put put = new Put(Bytes.toBytes(MailHelper.convertStringToMd5(key.toString())))
			put.addColumn(Feature.INFO, Feature.Qualifier.NAME, Bytes.toBytes(PersistenceConstant.Feature.SECONDARY))
			put.addColumn(VALUES, Feature.Qualifier.USUAL_EMAIL_TIME, Bytes.toBytes(key.toString()))
			put.addColumn(VALUES, Feature.Qualifier.AVERAGE_EMAIL_LENGTH, Bytes.toBytes(average))
			relevances.forEach { k, v -> put.addColumn(VALUES, Bytes.toBytes(k), Bytes.toBytes(calculateAverage(v))) }
			context.write(null, put)
		}

		double calculateAverage(List<Double> values) {
			return values.stream().mapToDouble { p -> p.doubleValue() }.average().getAsDouble()
		}
	}
}

if (new SecondaryFeature().run(null) == 1) {
	throw new IOException("Error to process the secondary feature")
}

