package com.bhvx.mail.test.persistence.dao;

public interface Dao<T>
{
	void save(T t);

	T findById(String id);
}
