package com.bhvx.mail.test.persistence.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Mail
{
	private String id;
	private byte[] originalFile;
}
