package com.bhvx.mail.test.persistence.dao;

import com.bhvx.mail.test.persistence.PersistenceConstant;
import com.bhvx.mail.test.persistence.model.Mail;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class MailDao extends HBaseDao implements Dao<Mail>
{
	@Autowired
	private Connection connection;

	@Override
	public void save(final Mail mail)
	{
		Table table = null;
		try
		{
			table = connection.getTable(PersistenceConstant.MailTable.TABLE);
			table.put(buildMailToPut(mail));
		}
		catch (IOException e)
		{
			throw new RuntimeException("Could not insert the mails table");
		}
		finally
		{
			closeConnection(table);

		}
	}

	@Override
	public Mail findById(final String id)
	{

		Table table = null;
		Mail mail = null;
		try
		{
			table = connection.getTable(PersistenceConstant.MailTable.TABLE);
			Get get = new Get(Bytes.toBytes(id));
			Result result = table.get(get);
			if (result.containsColumn(PersistenceConstant.MailTable.INFO, PersistenceConstant.MailTable.ORIGINAL_FILE))
			{
				byte[] value = result.getValue(PersistenceConstant.MailTable.INFO, PersistenceConstant.MailTable.ORIGINAL_FILE);
				mail = Mail.builder().originalFile(value).id(id).build();
			}
		}
		catch (IOException e)
		{
			throw new RuntimeException("Could not find mail " + id);
		}
		finally
		{
			closeConnection(table);
		}
		return mail;
	}

	private static Put buildMailToPut(final Mail mail)
	{
		final Put put = new Put(Bytes.toBytes(mail.getId()));
		put.addColumn(PersistenceConstant.MailTable.INFO, PersistenceConstant.MailTable.ORIGINAL_FILE, mail.getOriginalFile());
		return put;
	}
}
