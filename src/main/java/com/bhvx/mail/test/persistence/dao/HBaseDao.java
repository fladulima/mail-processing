package com.bhvx.mail.test.persistence.dao;

import org.apache.hadoop.hbase.client.Table;

import java.io.IOException;
import java.util.Objects;

public abstract class HBaseDao
{

	protected void closeConnection(final Table table)
	{
		if (Objects.nonNull(table))
		{
			try
			{
				table.close();
			}
			catch (IOException e)
			{
				throw new RuntimeException("Could not close the connection");
			}
		}
	}
}
