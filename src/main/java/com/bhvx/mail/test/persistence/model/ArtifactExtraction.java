package com.bhvx.mail.test.persistence.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ArtifactExtraction
{
	private String emailId;
	private String ticker;
	private String position;
}
