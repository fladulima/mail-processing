package com.bhvx.mail.test.persistence;

import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.util.Bytes;

public final class PersistenceConstant
{

	public static class MailTable
	{
		public static final TableName TABLE = TableName.valueOf("mails");
		public static final byte[] INFO = Bytes.toBytes("info");
		public static final byte[] ORIGINAL_FILE = Bytes.toBytes("original_file");
	}

	public static class ArtifactExtraction
	{
		public static final TableName TABLE = TableName.valueOf("artifact_extraction");
		public static final byte[] ARTIFACT = Bytes.toBytes("artifact");
		public static final byte[] TICKER = Bytes.toBytes("ticker");
		public static final byte[] POSITION = Bytes.toBytes("position");
	}

	public static class Feature
	{
		public static final TableName TABLE = TableName.valueOf("feature");
		public static final byte[] VALUES = Bytes.toBytes("values");
		public static final byte[] INFO = Bytes.toBytes("info");

		public static final String PRIMARY = "primary";
		public static final String SECONDARY = "secondary";

		public static class Qualifier
		{

			public static final byte[] NAME = Bytes.toBytes("name");

			public static final byte[] DATE_FEATURE = Bytes.toBytes("date");
			public static final byte[] EMAIL_LENGTH = Bytes.toBytes("emailLength");
			public static final String RELEVANCE_AS_STRING = "relevance";

			//secondary
			public static final byte[] USUAL_EMAIL_TIME = Bytes.toBytes("usualEmailTime");
			public static final byte[] AVERAGE_EMAIL_LENGTH = Bytes.toBytes("averageEmailLength");
		}
	}
}
