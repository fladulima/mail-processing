package com.bhvx.mail.test.persistence.dao;

import com.bhvx.mail.test.persistence.model.MailDocument;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface MailRepository extends ElasticsearchRepository<MailDocument, String>
{
}
