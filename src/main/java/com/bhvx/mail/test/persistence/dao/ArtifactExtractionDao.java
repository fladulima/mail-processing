package com.bhvx.mail.test.persistence.dao;

import com.bhvx.mail.test.persistence.PersistenceConstant;
import com.bhvx.mail.test.persistence.model.ArtifactExtraction;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class ArtifactExtractionDao extends HBaseDao implements Dao<ArtifactExtraction>
{
	private final static Logger LOGGER = Logger.getLogger(ArtifactExtractionDao.class.getName());

	@Autowired
	private Connection connection;

	@Override
	public void save(final ArtifactExtraction artifactExtraction)
	{
		Table table = null;
		try
		{
			table = connection.getTable(PersistenceConstant.ArtifactExtraction.TABLE);
			table.put(buildEntity(artifactExtraction));
		}
		catch (IOException e)
		{
			LOGGER.log(Level.SEVERE, "Error while saving the artifacts");
		}
		finally
		{
			closeConnection(table);
		}
	}

	@Override
	public ArtifactExtraction findById(final String id)
	{
		throw new RuntimeException("Not supported");
	}

	private static Put buildEntity(final ArtifactExtraction artifactExtraction)
	{
		final Put put = new Put(Bytes.toBytes(artifactExtraction.getEmailId()));
		put.addColumn(PersistenceConstant.ArtifactExtraction.ARTIFACT, PersistenceConstant.ArtifactExtraction.TICKER,
				Bytes.toBytes(artifactExtraction.getTicker()));
		put.addColumn(PersistenceConstant.ArtifactExtraction.ARTIFACT, PersistenceConstant.ArtifactExtraction.POSITION,
				Bytes.toBytes(artifactExtraction.getPosition()));
		return put;
	}
}
