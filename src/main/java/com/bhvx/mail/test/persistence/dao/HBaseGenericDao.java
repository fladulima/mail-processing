package com.bhvx.mail.test.persistence.dao;

import org.apache.hadoop.hbase.client.Put;

import java.util.List;
import java.util.Map;

public interface HBaseGenericDao
{
	void save(Put put);

	List<Map<String, Object>> findAll();

	List<Map<String, Object>> findByFeatureName(String name);
}
