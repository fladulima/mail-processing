package com.bhvx.mail.test.persistence.dao;

import com.bhvx.mail.test.persistence.PersistenceConstant;
import com.bhvx.mail.test.strategy.FeatureStrategy;
import org.apache.hadoop.hbase.CompareOperator;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.bhvx.mail.test.persistence.PersistenceConstant.Feature.TABLE;

@Component
public class FeatureDao extends HBaseDao implements HBaseGenericDao
{
	private final static Logger LOGGER = Logger.getLogger(ArtifactExtractionDao.class.getName());

	@Autowired
	private Connection connection;

	@Autowired
	private Map<String, FeatureStrategy> featureStrategyMap;

	@Override
	public void save(final Put put)
	{
		Table table = null;
		try
		{
			table = connection.getTable(TABLE);
			table.put(put);
		}
		catch (IOException e)
		{
			LOGGER.log(Level.SEVERE, "Error while saving the feature", e.getMessage());
		}
		finally
		{
			closeConnection(table);
		}
	}

	@Override
	public List<Map<String, Object>> findAll()
	{
		Table table = null;
		final List<Map<String, Object>> data = new ArrayList<>();
		try
		{
			table = connection.getTable(PersistenceConstant.Feature.TABLE);
			Scan scan = new Scan();
			ResultScanner results = table.getScanner(scan);

			for (Result result : results)
			{
				byte[] name = result.getValue(PersistenceConstant.Feature.INFO, PersistenceConstant.Feature.Qualifier.NAME);
				final FeatureStrategy featureStrategy = featureStrategyMap.get(Bytes.toString(name));

				if (Objects.nonNull(featureStrategy))
				{
					data.add(featureStrategy.getFeatureValues(result));
				}
			}
			results.close();
		}
		catch (IOException e)
		{
			LOGGER.log(Level.SEVERE, "Error while scanning the feature", e.getMessage());
		}
		finally
		{
			closeConnection(table);
		}
		return data;
	}

	@Override
	public List<Map<String, Object>> findByFeatureName(final String featureName)
	{
		Table table = null;
		final List<Map<String, Object>> data = new ArrayList<>();
		try
		{
			table = connection.getTable(PersistenceConstant.Feature.TABLE);
			Scan scan = new Scan();
			SingleColumnValueFilter singleColumnValueFilter = new SingleColumnValueFilter(PersistenceConstant.Feature.INFO,
					PersistenceConstant.Feature.Qualifier.NAME, CompareOperator.EQUAL, new BinaryComparator(Bytes.toBytes(featureName)));
			scan.setFilter(singleColumnValueFilter);

			ResultScanner results = table.getScanner(scan);

			for (Result result : results)
			{
				byte[] name = result.getValue(PersistenceConstant.Feature.INFO, PersistenceConstant.Feature.Qualifier.NAME);
				final FeatureStrategy featureStrategy = featureStrategyMap.get(Bytes.toString(name));

				if (Objects.nonNull(featureStrategy))
				{
					data.add(featureStrategy.getFeatureValues(result));
				}
			}
			results.close();
		}
		catch (IOException e)
		{
			LOGGER.log(Level.SEVERE, "Error while scanning the feature", e.getMessage());
		}
		finally
		{
			closeConnection(table);
		}
		return data;
	}
}
