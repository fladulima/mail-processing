package com.bhvx.mail.test.tasklet;

import com.bhvx.mail.test.persistence.dao.ArtifactExtractionDao;
import com.bhvx.mail.test.persistence.model.ArtifactExtraction;
import com.bhvx.mail.test.persistence.model.MailDocument;
import com.jayway.jsonpath.JsonPath;
import org.elasticsearch.action.termvectors.TermVectorsRequest;
import org.elasticsearch.action.termvectors.TermVectorsResponse;
import org.elasticsearch.common.bytes.BytesReference;
import org.elasticsearch.common.xcontent.ToXContent;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentHelper;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;

@Component
public class ArtifactExtractTasklet implements Tasklet
{
	private final static Logger LOGGER = Logger.getLogger(ArtifactExtractTasklet.class.getName());

	@Value("${artifact.list}")
	private String artifactList;

	@Autowired
	private ElasticsearchTemplate template;

	@Autowired
	private ArtifactExtractionDao artifactExtractionDao;

	@Override
	public RepeatStatus execute(final StepContribution conxtribution, final ChunkContext chunkContext)
	{
		Stream.of(artifactList.split(",")).map(String::toLowerCase).forEach(ticker -> {
			final SearchQuery query = new NativeSearchQueryBuilder().withQuery(matchQuery("body", ticker)).build();

			template.queryForList(query, MailDocument.class)
					.stream()
					.forEach(mailDocument -> getTermPositions(mailDocument, ticker).stream().forEach(pos -> {
						ArtifactExtraction artifactExtraction = ArtifactExtraction.builder()
								.emailId(mailDocument.getId())
								.ticker(ticker)
								.position(String.valueOf(pos))
								.build();

						artifactExtractionDao.save(artifactExtraction);
					}));

		});
		return RepeatStatus.FINISHED;
	}

	protected List<Integer> getTermPositions(final MailDocument mailDocument, final String ticker)
	{
		final String id = mailDocument.getId();
		List<Integer> positions = Collections.emptyList();
		final TermVectorsRequest request = new TermVectorsRequest().id(id).type("content").index("mail").selectedFields("body");
		try
		{
			final TermVectorsResponse response = template.getClient().termVectors(request).actionGet();
			XContentBuilder builder = jsonBuilder();
			response.toXContent(builder, ToXContent.EMPTY_PARAMS);
			final Map<String, Object> value = XContentHelper.convertToMap(BytesReference.bytes(builder), false, builder.contentType())
					.v2();

			positions = JsonPath.parse(value).read("term_vectors.body.terms." + ticker + ".tokens[*].position");
		}
		catch (Exception e)
		{
			LOGGER.log(Level.SEVERE, "Error while transforming the position");
		}

		return positions;
	}

	public void setArtifactList(final String artifactList)
	{
		this.artifactList = artifactList;
	}

	public void setTemplate(final ElasticsearchTemplate template)
	{
		this.template = template;
	}

	public void setArtifactExtractionDao(final ArtifactExtractionDao artifactExtractionDao)
	{
		this.artifactExtractionDao = artifactExtractionDao;
	}
}
