package com.bhvx.mail.test.tasklet;

import com.bhvx.mail.test.persistence.PersistenceConstant;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptor;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptorBuilder;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.TableDescriptorBuilder;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

@Component
public class HBaseInitTableTasklet implements Tasklet
{
	@Override
	public RepeatStatus execute(final StepContribution contribution, final ChunkContext chunkContext) throws Exception
	{
		final Configuration conf = HBaseConfiguration.create();
		final Connection connection = ConnectionFactory.createConnection(conf);
		final Admin admin = connection.getAdmin();
		final TableName mails = PersistenceConstant.MailTable.TABLE;
		if (!admin.tableExists(mails))
		{
			final ColumnFamilyDescriptor columnFamilyDescriptor = ColumnFamilyDescriptorBuilder.newBuilder(
					PersistenceConstant.MailTable.INFO).setMobEnabled(true).setMobThreshold(102400L).
					build();

			final TableDescriptorBuilder tableDescriptorBuilder = TableDescriptorBuilder.newBuilder(mails)
					.setColumnFamily(columnFamilyDescriptor);

			admin.createTable(tableDescriptorBuilder.build());
		}

		final TableName artifactExtraction = PersistenceConstant.ArtifactExtraction.TABLE;
		if (!admin.tableExists(artifactExtraction))
		{
			final ColumnFamilyDescriptor columnFamilyDescriptor = ColumnFamilyDescriptorBuilder.newBuilder(
					PersistenceConstant.ArtifactExtraction.ARTIFACT).build();
			final TableDescriptorBuilder tableDescriptorBuilder = TableDescriptorBuilder.newBuilder(artifactExtraction)
					.setColumnFamily(columnFamilyDescriptor);
			admin.createTable(tableDescriptorBuilder.build());
		}

		final TableName feature = PersistenceConstant.Feature.TABLE;
		if (!admin.tableExists(feature))
		{
			final ColumnFamilyDescriptor value = ColumnFamilyDescriptorBuilder.newBuilder(PersistenceConstant.Feature.VALUES).build();
			final ColumnFamilyDescriptor info = ColumnFamilyDescriptorBuilder.newBuilder(PersistenceConstant.Feature.INFO).build();
			final TableDescriptorBuilder featureTableBuilder = TableDescriptorBuilder.newBuilder(feature).setColumnFamily(value).setColumnFamily(info);
			admin.createTable(featureTableBuilder.build());
		}

		return RepeatStatus.FINISHED;
	}
}
