package com.bhvx.mail.test.batch;

import com.bhvx.mail.test.persistence.dao.MailRepository;
import com.bhvx.mail.test.persistence.model.MailDocument;
import org.elasticsearch.common.CheckedBiFunction;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.bhvx.mail.test.util.MailHelper.buildEntityFromFile;
import static com.bhvx.mail.test.util.MailHelper.convertStringToMd5;

@Component
public class EsItemWriter implements ItemWriter<File>
{
	private MailRepository mailRepository;

	public EsItemWriter(final MailRepository mailRepository)
	{
		this.mailRepository = mailRepository;
	}

	@Override
	public void write(final List<? extends File> items)
	{
		final CheckedBiFunction<File, MimeMessage, MailDocument, Exception> mailFunction = (file, message) -> MailDocument.builder()
				.body(Objects.nonNull(message.getContent()) ? message.getContent().toString() : null)
				.id(convertStringToMd5(message.getMessageID()))
				.build();

		final List<MailDocument> documents = items.stream()
				.map(file -> buildEntityFromFile(file, mailFunction))
				.filter(Objects::nonNull)
				.collect(Collectors.toList());

		mailRepository.saveAll(documents);
	}
}
