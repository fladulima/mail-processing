package com.bhvx.mail.test.batch;

import org.springframework.batch.item.ItemReader;

import java.io.File;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class MultiFileItemReader implements ItemReader<File>
{
	private Iterator<File> files;

	public MultiFileItemReader(List<File> files)
	{
		this.files = files != null ? files.iterator() : Collections.emptyIterator();
	}

	@Override
	public File read()
	{
		if (this.files.hasNext())
		{
			return this.files.next();
		}
		return null;
	}
}
