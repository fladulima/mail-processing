package com.bhvx.mail.test.batch;

import com.bhvx.mail.test.persistence.dao.MailDao;
import com.bhvx.mail.test.persistence.model.Mail;
import org.elasticsearch.common.CheckedBiFunction;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.nio.file.Files;
import java.util.List;
import java.util.Objects;

import static com.bhvx.mail.test.util.MailHelper.buildEntityFromFile;
import static com.bhvx.mail.test.util.MailHelper.convertStringToMd5;

@Component
public class HBaseItemWriter implements ItemWriter<File>
{
	private MailDao mailDao;

	public HBaseItemWriter(final MailDao mailDao)
	{
		this.mailDao = mailDao;
	}

	@Override
	public void write(final List<? extends File> files)
	{

		final CheckedBiFunction<File, MimeMessage, Mail, Exception> mailFunction = (file, message) -> Mail.builder()
				.id(convertStringToMd5(message.getMessageID()))
				.originalFile(Files.readAllBytes(file.toPath()))
				.build();

		files.stream().map(file -> buildEntityFromFile(file, mailFunction)).filter(Objects::nonNull).forEach(mailDao::save);
	}
}
