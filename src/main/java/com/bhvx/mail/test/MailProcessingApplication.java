package com.bhvx.mail.test;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@SpringBootApplication
@EnableBatchProcessing
@EnableElasticsearchRepositories(basePackages = "com.bhvx.mail.test.persistence")
public class MailProcessingApplication
{

	public static void main(String[] args)
	{
		SpringApplication.run(MailProcessingApplication.class, args);
	}

}
