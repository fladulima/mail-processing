package com.bhvx.mail.test.config;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class HBaseConfig
{

	@Bean
	protected Connection connection()
	{
		try
		{
			return ConnectionFactory.createConnection();
		}
		catch (IOException e)
		{
			throw new RuntimeException("Could not create connection");
		}
	}
}
