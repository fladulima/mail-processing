package com.bhvx.mail.test.config;

import com.bhvx.mail.test.service.ScriptFeatureExecutionService;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeatureFlowJobConfig
{
	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private ScriptFeatureExecutionService scriptFeatureExecutionService;

	@Bean
	public Step featureExecutionStep()
	{
		return stepBuilderFactory.get("featureExecutionStep").tasklet((contribution, chunkContext) -> {
			scriptFeatureExecutionService.executeFeature();
			return RepeatStatus.FINISHED;
		}).build();
	}

	@Bean
	public Flow featuresFlow()
	{
		final FlowBuilder<Flow> flowBuilder = new FlowBuilder("featuresFlow");
		flowBuilder.start(featureExecutionStep()).end();
		return flowBuilder.build();
	}
}
