package com.bhvx.mail.test.config;

import com.bhvx.mail.test.batch.EsItemWriter;
import com.bhvx.mail.test.batch.HBaseItemWriter;
import com.bhvx.mail.test.batch.MultiFileItemReader;
import com.bhvx.mail.test.tasklet.ArtifactExtractTasklet;
import com.bhvx.mail.test.tasklet.HBaseInitTableTasklet;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Integer.MAX_VALUE;

@Configuration
public class IngestDataFlowJobConfig
{
	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private HBaseInitTableTasklet setupTask;

	@Autowired
	private JobLauncher jobLauncher;

	@Autowired
	private HBaseItemWriter hBaseItemWriter;

	@Autowired
	private ArtifactExtractTasklet artifactExtractTasklet;

	@Autowired
	private EsItemWriter hsEsItemWriter;

	@Autowired
	private Flow featuresFlow;

	@Value("${mail.dataset.directory}")
	private String mailDataSet;

	@Value("${mail.chunksize:200}")
	private int chunkSize;

	@Bean
	public Job job()
	{
		return jobBuilderFactory.get("job")
				.start(initTable())
				.next(ingestDataStep())
				.next(artifactExtraction())
				.on("COMPLETED")
				.to(featuresFlow)
				.end()
				.build();
	}

	@Bean
	public Step initTable()
	{
		return stepBuilderFactory.get("initTable").tasklet(setupTask).build();
	}

	@Bean
	public Step artifactExtraction()
	{
		return stepBuilderFactory.get("artifactExtraction").tasklet(artifactExtractTasklet).build();
	}

	@Bean
	public Step ingestDataStep()
	{
		return stepBuilderFactory.get("ingestDataStep").<File, File>chunk(chunkSize).reader(multiFileReader())
				.writer(compositeItemWriter())
				.taskExecutor(new SimpleAsyncTaskExecutor())
				.build();
	}

	@Bean
	public CompositeItemWriter compositeItemWriter()
	{
		final CompositeItemWriter compositeItemWriter = new CompositeItemWriter();
		compositeItemWriter.setDelegates(Arrays.asList(hBaseItemWriter, hsEsItemWriter));
		return compositeItemWriter;
	}

	@Bean
	public MultiFileItemReader multiFileReader()
	{
		final List<File> files;
		try
		{
			files = Files.walk(Paths.get(mailDataSet), MAX_VALUE)
					.map(Path::toFile)
					.filter(file -> !file.isHidden())
					.filter(File::isFile)
					.collect(Collectors.toList());
		}
		catch (IOException e)
		{
			throw new RuntimeException("Could not read the dataset directory");
		}
		return new MultiFileItemReader(files);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void startJob() throws Exception
	{
		jobLauncher.run(job(), new JobParameters());
	}
}
