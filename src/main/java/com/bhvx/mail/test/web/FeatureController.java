package com.bhvx.mail.test.web;

import com.bhvx.mail.test.service.FeatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/features")
public class FeatureController
{

	@Autowired
	private FeatureService featureService;

	@GetMapping
	public ResponseEntity<?> getAll()
	{
		return ResponseEntity.ok(featureService.findAll());
	}

	@GetMapping("/{name}")
	public ResponseEntity<?> getByFeatureName(@PathVariable String name)
	{
		return ResponseEntity.ok(featureService.findByFeatureName(name));
	}
}
