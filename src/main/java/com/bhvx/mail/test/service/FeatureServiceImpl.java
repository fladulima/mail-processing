package com.bhvx.mail.test.service;

import com.bhvx.mail.test.persistence.dao.FeatureDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class FeatureServiceImpl implements FeatureService
{
	@Autowired
	private FeatureDao featureDao;

	@Override
	public List<Map<String, Object>> findAll()
	{
		return featureDao.findAll();
	}

	@Override
	public List<Map<String, Object>> findByFeatureName(final String name)
	{
		return featureDao.findByFeatureName(name);
	}
}
