package com.bhvx.mail.test.service;

import java.util.List;
import java.util.Map;

public interface FeatureService
{
	List<Map<String, Object>> findAll();

	List<Map<String, Object>> findByFeatureName(final String name);
}
