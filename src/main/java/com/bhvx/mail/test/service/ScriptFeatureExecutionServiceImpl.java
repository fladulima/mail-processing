package com.bhvx.mail.test.service;

import com.bhvx.mail.test.persistence.dao.ArtifactExtractionDao;
import com.bhvx.mail.test.persistence.dao.HBaseGenericDao;
import com.bhvx.mail.test.persistence.dao.MailDao;
import groovy.lang.GroovyClassLoader;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Component;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import static java.util.Arrays.stream;

@Component
public class ScriptFeatureExecutionServiceImpl implements ScriptFeatureExecutionService
{
	@Autowired
	private ElasticsearchTemplate elasticsearchTemplate;

	@Autowired
	private HBaseGenericDao featureDao;

	@Autowired
	private MailDao mailDao;

	@Autowired
	private ArtifactExtractionDao artifactExtractionDao;

	@Value("${artifact.list}")
	private String artifactList;

	@Value("${mail.features.dir}")
	private String defaultFeatures;

	@Override
	public void executeFeature()
	{
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		final GroovyClassLoader groovyClassLoader = new GroovyClassLoader(contextClassLoader);
		final ScriptEngineManager manager = new ScriptEngineManager(groovyClassLoader);
		final ScriptEngine engine = manager.getEngineByName("groovy");

		final Map<String, Object> params = new HashMap<>();
		params.put("artifactList", artifactList.split(","));
		engine.put("elasticsearchTemplate", elasticsearchTemplate);
		engine.put("featureDao", featureDao);
		engine.put("artifactExtractionDao", artifactExtractionDao);
		engine.put("mailDao", mailDao);
		engine.put("parameters", params);

		stream(defaultFeatures.split(";")).flatMap(value -> stream(value.split(","))).forEach(filePath -> {

			final File file = new File(filePath);
			if (file.isFile() && FilenameUtils.getExtension(file.getName()).equals("groovy"))
			{
				try
				{
					final Class aClass = groovyClassLoader.parseClass(file);
					Thread.currentThread().setContextClassLoader(groovyClassLoader);
					Thread.currentThread().getContextClassLoader().loadClass(aClass.getName());
					engine.eval(new FileReader(file));
				}
				catch (Exception e)
				{
					throw new RuntimeException("Could not execute the feature calculation", e);
				}
			}
		});
	}
}
