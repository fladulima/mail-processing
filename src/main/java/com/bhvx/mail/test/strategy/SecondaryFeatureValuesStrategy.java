package com.bhvx.mail.test.strategy;

import com.bhvx.mail.test.persistence.PersistenceConstant;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.bhvx.mail.test.persistence.PersistenceConstant.Feature.Qualifier.AVERAGE_EMAIL_LENGTH;
import static com.bhvx.mail.test.persistence.PersistenceConstant.Feature.Qualifier.RELEVANCE_AS_STRING;

@Component(PersistenceConstant.Feature.SECONDARY)
public class SecondaryFeatureValuesStrategy implements FeatureStrategy
{
	@Override
	public Map<String, Object> getFeatureValues(final Result result)
	{
		final Map<String, Object> map = new HashMap<>();
		byte[] name = result.getValue(PersistenceConstant.Feature.INFO, PersistenceConstant.Feature.Qualifier.NAME);
		map.put("name", Bytes.toString(name));
		result.getFamilyMap(PersistenceConstant.Feature.VALUES).forEach((k, v) -> {
			String valueAsString = Bytes.toString(v);
			if (Bytes.toString(k).contains(RELEVANCE_AS_STRING))
			{
				valueAsString = String.valueOf(Bytes.toFloat(v));
			}

			if (Bytes.toString(k).equals(Bytes.toString(AVERAGE_EMAIL_LENGTH)))
			{
				valueAsString = Bytes.toDouble(v)+"";
			}
			map.put(Bytes.toString(k), valueAsString);
		});

		return map;
	}
}
