package com.bhvx.mail.test.strategy;

import org.apache.hadoop.hbase.client.Result;

import java.util.Map;

public interface FeatureStrategy
{
	Map<String, Object> getFeatureValues(Result result);
}
