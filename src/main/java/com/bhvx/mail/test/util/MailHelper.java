package com.bhvx.mail.test.util;

import org.elasticsearch.common.CheckedBiFunction;
import org.springframework.util.DigestUtils;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MailHelper
{
	private final static Logger LOGGER = Logger.getLogger(MailHelper.class.getName());
	private final static Session mailSession = Session.getDefaultInstance(new Properties());

	public static <T> T buildEntityFromFile(File file, CheckedBiFunction<File, MimeMessage, T, Exception> function)
	{
		T result = null;
		try (InputStream source = new FileInputStream(file))
		{
			final MimeMessage message = new MimeMessage(mailSession, source);
			result = function.apply(file, message);
		}
		catch (Exception e)
		{
			LOGGER.log(Level.SEVERE, "Error while trying to parse the mail");
		}
		return result;
	}

	public static String convertStringToMd5(final String value)
	{
		return DigestUtils.md5DigestAsHex(value.getBytes(StandardCharsets.UTF_8));
	}

	public static Date getDateFromBytes(final byte[] value)
	{
		Date date = null;
		try (InputStream source = new ByteArrayInputStream(value))
		{
			final MimeMessage message = new MimeMessage(mailSession, source);

			date = message.getSentDate();

		}
		catch (Exception e)
		{
			LOGGER.log(Level.SEVERE, "Error while trying to parse the mail");
		}

		return date;
	}

}
