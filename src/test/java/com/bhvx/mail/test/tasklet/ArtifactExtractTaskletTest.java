package com.bhvx.mail.test.tasklet;

import com.bhvx.mail.test.persistence.dao.ArtifactExtractionDao;
import com.bhvx.mail.test.persistence.model.ArtifactExtraction;
import com.bhvx.mail.test.persistence.model.MailDocument;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.SearchQuery;

import java.util.Arrays;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ArtifactExtractTaskletTest
{

	public static final String TICKER = "sbel";
	@Spy
	private ArtifactExtractTasklet tasklet = new ArtifactExtractTasklet();

	@Mock
	private ElasticsearchTemplate template;

	@Mock
	private ArtifactExtractionDao artifactExtractionDao;

	@Captor
	private ArgumentCaptor<ArtifactExtraction> argumentCaptor;

	@Before
	public void setUp() throws Exception
	{
		tasklet.setTemplate(template);
		tasklet.setArtifactExtractionDao(artifactExtractionDao);
	}

	@Test
	public void shouldExtractTasklet()
	{
		//arrange
		tasklet.setArtifactList(TICKER);
		MailDocument mailDocument = MailDocument.builder().id("id").body("dummy body sbel").build();
		when(template.queryForList(any(SearchQuery.class), any())).thenReturn(Arrays.asList(mailDocument));
		doReturn(Arrays.asList(4)).when(tasklet).getTermPositions(mailDocument, TICKER);

		//act
		tasklet.execute(null, null);

		//assert
		verify(artifactExtractionDao, times(1)).save(argumentCaptor.capture());
		ArtifactExtraction artifactExtraction = argumentCaptor.getValue();
		assertThat(artifactExtraction.getPosition(), Matchers.notNullValue());
		assertThat(artifactExtraction.getEmailId(), Matchers.notNullValue());
		assertThat(artifactExtraction.getTicker(), Matchers.notNullValue());
	}
}