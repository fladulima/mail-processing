package com.bhvx.mail.test.batch;

import com.bhvx.mail.test.persistence.dao.MailRepository;
import com.bhvx.mail.test.persistence.model.MailDocument;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class EsItemWriterTest
{

	private EsItemWriter esItemWriter;

	@Mock
	private MailRepository mailRepository;

	@Captor
	private ArgumentCaptor<List<MailDocument>> argumentCaptor;

	@Before
	public void setUp()
	{
		esItemWriter = new EsItemWriter(mailRepository);
	}

	@Test
	public void shouldSaveAllDocuments()
	{
		//arrange
		File file = new File(getClass().getClassLoader().getResource("inbox/1.").getFile());

		//act
		esItemWriter.write(Arrays.asList(file, file, file));

		//assert
		verify(mailRepository, times(1)).saveAll(argumentCaptor.capture());
		List<MailDocument> value = argumentCaptor.getValue();
		assertThat(value.size(), Matchers.is(3));
		MailDocument mailDocument = value.get(0);
		assertThat(mailDocument.getId(), notNullValue());
		assertThat(mailDocument.getBody(), notNullValue());
	}
}