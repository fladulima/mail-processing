package com.bhvx.mail.test.batch;

import com.bhvx.mail.test.persistence.dao.MailDao;
import com.bhvx.mail.test.persistence.model.Mail;
import com.bhvx.mail.test.persistence.model.MailDocument;
import org.elasticsearch.common.CheckedBiFunction;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.bhvx.mail.test.util.MailHelper.buildEntityFromFile;
import static com.bhvx.mail.test.util.MailHelper.convertStringToMd5;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class HBaseItemWriterTest
{

	private HBaseItemWriter hBaseItemWriter;

	@Mock
	private MailDao mailDao;
	@Captor
	private ArgumentCaptor<Mail> argumentCaptor;

	@Before
	public void setUp()
	{
		hBaseItemWriter = new HBaseItemWriter(mailDao);
	}

	@Test
	public void shouldSaveToHbase()
	{
		//arrange
		File file = new File(getClass().getClassLoader().getResource("inbox/1.").getFile());

		//act
		hBaseItemWriter.write(Arrays.asList(file, file, file));

		//assert
		verify(mailDao, times(3)).save(argumentCaptor.capture());
		Mail value = argumentCaptor.getAllValues().get(0);
		assertThat(value.getId(), notNullValue());
		assertThat(value.getOriginalFile(), notNullValue());

	}
}