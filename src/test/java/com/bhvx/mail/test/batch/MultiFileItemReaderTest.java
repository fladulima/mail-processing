package com.bhvx.mail.test.batch;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;

public class MultiFileItemReaderTest
{
	private MultiFileItemReader multiFileItemReader;

	@Test
	public void shouldReadFile() throws IOException
	{
		Path dummy1 = Files.createTempFile("dummy", ".tmp");
		Path dummy2 = Files.createTempFile("dummy1", ".tmp");
		multiFileItemReader = new MultiFileItemReader(Arrays.asList(dummy1.toFile(), dummy2.toFile()));
		List<File> result = Arrays.asList(multiFileItemReader.read(), multiFileItemReader.read());
		assertThat(result, containsInAnyOrder(dummy1.toFile(), dummy2.toFile()));
	}
}